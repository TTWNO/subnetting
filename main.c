#include <stdio.h>
#include <errno.h>
#include "subnetting.h"

int main(){
	// actual program
	char* print_format_headers = {"%18s | %31s | %18s | %16s | /%6s\n"};
	char* print_format = {"%18s | %31s | %18s | %16s | /%2d\n"};
	int num_of_networks;
	int hosts_per_network;
	char file_location[255];
	FILE* print_file;
	printf("Where would you like to save these tables? ");
	if (scanf("%s", file_location) != 1){
	  fprintf(stderr, "There was an error processing the filename!\n");
	}
	print_file = fopen(file_location, "w");
	fprintf(print_file, "----------------------------------------------------------------------------------------------------\n");
	printf("How many networks do you need? ");
	if (scanf("%d", &num_of_networks) != 1){
	  fprintf(stderr, "There was an error reading the number of networks!\n");
	}
	char ip[16];
	unsigned int ui_ip;

	printf("Enter a base IP address to start configuring. (Usually 192.168.x.0, 172.17.x.0, etc...)\nBase IP: ");
	
	if (scanf("%s", ip) != 1){	
	  fprintf(stderr, "There was an error reading the base IP address!\n");
	}
	ui_ip = octets_to_ui(ip);
	fprintf(print_file, print_format_headers, "Network Address", "Host Range", "Broadcast Address", "Subnet Mask", "prefix");
	for (int i = 0; i < num_of_networks; ++i){
		int hosts;
		char subnt_s[16];
		char broadcast_addr[16];
		char network_addr[16];
		char usable_range[31];
		unsigned int subnt;
		int this_prefix;
		printf("How many hosts do you need on this network [%d]? ", i);
		if (scanf("%d", &hosts) != 1){
			fprintf(stderr, "Errpr readomg number of hosts!\n");
		}
		this_prefix = prefix_for_hosts(hosts);
		prefix_to_mask(subnt_s, this_prefix);
		subnt = octets_to_ui(subnt_s);
		broadcast_address(broadcast_addr, ip, subnt_s);
		network_address(network_addr, ip, subnt_s);
		usable_host_addresses(usable_range, ip, subnt_s);
		fprintf(print_file, print_format, network_addr, usable_range, broadcast_addr, subnt_s, this_prefix);
		ui_ip = octets_to_ui(broadcast_addr) + 1;	
		ui_to_octets(ip, ui_ip);
	}
	return 0;
}
