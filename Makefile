CC?=gcc
default: build

all: build tewt

subnetting.o:
	${CC} -c -o subnetting.o subnetting.c -lm -O2

build: subnetting.o
	${CC} -o subnetting subnetting.o main.c -lm -O2
	
test: subnetting.o
	${CC} -o test subnetting.o main.c -lm -O2

clean:
	rm subnetting
	rm *o
	rm test
