#include <stdio.h>
#include "subnetting.h"



int main(){
	
	char* mask1 = {"255.255.255.192"};
	char mask2[16];
	int prefix, hosts, networks;
	unsigned int octets[4];

	prefix = mask_to_prefix(mask1);
	fprintf(stdout, "Prefix of %s is %d\n", mask1, prefix);
	prefix_to_mask(mask2, prefix);
	fprintf(stdout, "Subnet of %d is %s\n", prefix, mask2);
	hosts = hosts_for_prefix(prefix);
	printf("It can support up to %d hosts.\n", hosts);
	networks = subnets_for_prefix(prefix);
	printf("It can support ip to %d subnets.\n", networks);
	subnet_to_octet_array(octets, mask1);
	for (int i = 0; i < 4; ++i){
		printf("\tPt %d: %d\n", i+1, octets[i]);
	}

	int prefix64,prefix2000;
	char subnet64[16];
	char subnet2000[16];

	int subarr64[4];
	int subarr2000[4];

	prefix64 = prefix_for_hosts(64);
	prefix2000 = prefix_for_hosts(2000);
	prefix_to_mask(subnet64, prefix64);
	prefix_to_mask(subnet2000, prefix2000);
	subnet_to_octet_array(subarr64, subnet64);
	subnet_to_octet_array(subarr2000, subnet2000);
	printf("64 hosts requires a prefix of %d (%s).\n", prefix64, subnet64);
	printf("2000 hosts requires a prefix of %d (%s).\n", prefix2000, subnet2000);
	printf("A prefix of %d can support %d hosts.\n", prefix2000, hosts_for_prefix(prefix2000));
	printf("%s indicates a class %c network.\n", subnet64, network_class(subarr64));
	printf("%s indicates a class %c network.\n", subnet2000, network_class(subarr2000));
	

	char* ip1 = {"192.168.16.122"};
	char* sub1_ip1 = { "255.255.255.240" };
	char na_ip1[16];
	char ba_ip1[16];
	char ra_ip1[33];
	network_address(na_ip1, ip1, sub1_ip1);
	broadcast_address(ba_ip1, ip1, sub1_ip1);
	usable_host_addresses(ra_ip1, ip1, sub1_ip1);
	printf("%s (%s)'s network address is %s.\n", ip1, sub1_ip1, na_ip1);
	printf("%s (%s)'s broadcast address is %s.\n", ip1, sub1_ip1, ba_ip1);
	printf("The host range is %s\n", ra_ip1);
	char* ip2 = {"172.17.130.222"};
}
