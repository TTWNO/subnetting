#ifndef SUBNETTING_H
#define SUBNETTING_H
char* prefix_to_mask(char subnet_mask[16], unsigned int prefix);
int hosts_for_prefix(int prefix);
int subnets_for_prefix(int prefix);
void ui_to_octets(char addr[16], unsigned int subnet_mask);
int prefix_for_hosts(int hosts_required);
int network_type_based_on_subnet(int subnet_arr[4]);
char network_class(int subnet_arr[4]);
char* network_address(char result[16], char ip[16], char subnet[16]);
char* broadcast_address(char result[16], char ip[16], char subnet[16]);
char* usable_host_addresses(char result[33], char ip[16], char subnet[16]);
int get_ui_class(unsigned int ip);
unsigned int* subnet_to_octet_array(unsigned int octets[4], char subnet_mask[16]);
unsigned int octets_to_ui(char octets[16]);
#endif
